import {
  Template
} from 'meteor/templating';
import {
  ReactiveVar
} from 'meteor/reactive-var';

import {
  all,
  add,
  toggle,
  remove
} from '../api/tasks.js';

import './main.html';
import {
  removeAllListeners
} from 'cluster';

Template.body.helpers({
  tasks() {
    return all();
  },
});

Template.body.events({
  'submit #add'(event) {
    event.preventDefault()

    const target = event.target;

    const text = target.text.value;

    add({
      text,
      createdAt: new Date()
    })

    target.text.value = '';
  },
})

Template.task.events({
  'click .toggle-checked'() {
    toggle(this)
  },
  'click .delete'() {
    remove(this)
  },
})