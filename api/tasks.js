import {
    Mongo
} from 'meteor/mongo';

export const Tasks = new Mongo.Collection('tasks');

export function all() {
    return Tasks.find({});
}

export function add(task) {
    Tasks.insert(task);
}

export function toggle(task) {
    Tasks.update(task._id, {
        $set: {
            checked: !task.checked
        },
    });
}

export function remove(task) {
    Tasks.remove(task._id)
}